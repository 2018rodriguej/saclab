def test_index_page_return_righ_message(self):
    request = HttpRequest()
    response = index(request)
    html = response.content.decode('utf8')
    message = "Bienvenue sur SACLAB"
    self.assertTrue(message in html)

def url_resolve_to_index_view(self):
    found = resolve('/projects/')
    self.assertEqual(found.func, index)