    def test_url_resolve_edit_view(self):
        for k in range (100):
            found = resolve('/projects/edit/'+str(k)+'/')
            self.assertEqual(found.func, edit_project)

    def test_edit_view_returns_right_message(self):
        request = HttpRequest()
        for k in range (100):
            response = edit_project(request, k)
            html = response.content.decode('utf8')
            if not Project.objects.filter(id=k):
                message = 'Erreur'
                self.assertTrue(message in html)
            elif request.method == 'POST':
                message = 'Le projet a été édité avec succès'
                self.assertTrue(message in html)
            else:
                message = 'Editer le projet'
                self.assertTrue(message in html)