#####################
# ==>projects/urls.py
#####################

urlpatterns=[
    # ...
    url(r'^view/(?P<project_id>[0-9]+)/$', views.afficher_projet, name='afficher_projet'),  #affiche le détail d'un projet
]

########################
# ==>projects/views.py
########################

from django.http import HttpResponse
from projects.models import Project

def afficher_projets(request, project_id):
    message = "Vous regardez le projet n°" + str(project_id)
    project = Project.objects.get(id=project_id)
    message += " intitulé " + str(project.projectname) + "."
    message += "<br/><br/>Description:<br/>" + str(project.description)
    return HttpResponse(message)


def test_url_projects_view(self):
    b=False
    for project in Project.objects.all():
        found = resolve('/projects/view/'+str(project.id)+'/')
        if found.func == afficher_projets:
            b=True
    self.assertTrue(b)

def test_page_projects_view(self):
    b=False
    for project in Projects.objects.all():    
        request = HttpRequest()
        response = afficher_projets(request, projects.id)
        html = response.content.decode('utf8')
        self.assertTrue(project.projectname in html)
        self.assertTrue(project.description in html)
    ids=[0,-1,len(Projects.objects.all())]
    for id in ids:
        self.assertTrue("Erreur : ce projet n'existe pas." in html)