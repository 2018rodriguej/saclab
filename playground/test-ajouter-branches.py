def test_url_resolve_view_ajouter_branche(self):
    User(username="Lucas", password="osef").save()
    Project(projectname="Machin", description="bidule").save()
    Project(projectname="Machin2", description="bidule2").save()
    Branch(branchname="Blabla", project = Project.objects.get(projectname="Machin")).save()
    Branch(branchname="Blabla2", project = Project.objects.get(projectname="Machin2")).save()
    for k in range (len(Project.objects.all())):
        found = resolve ('/projects/add/'+str(k+1)+'/')
        self.assertEqual(found.func, ajouter_branche)
    
def test_ajouter_branche_returns_right_message(self):
    User(username="Lucas", password="osef").save()
    Project(projectname="Machin", description="bidule").save()
    Project(projectname="Machin2", description="bidule2").save()
    Branch(branchname="Blabla", project = Project.objects.get(projectname="Machin")).save()
    Branch(branchname="Blabla2", project = Project.objects.get(projectname="Machin2")).save()
    request = HttpRequest()
    for k in range (len(Project.objects.all())):
        response = ajouter_branche(request, k)
        html = response.content.decode('utf8')
        project = Project.objects.filter(id=k+1)
        if not project:
            message = "Erreur"
            self.assertTrue(message in html)
        elif request.method == 'POST':
            branchname = request.POST.get("name")
            if not branchname:
                message = "Erreur"
                self.assertTrue(message in html)
            else:
                message = "La nouvelle branche a été créée"
                self.assertTrue(message in html)