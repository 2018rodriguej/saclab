class afficher_branche_page_tests(TestCase):


    def test_afficher_branche_page(self):
            utilisateur = User(username="Lucas", password="osef")
            utilisateur.save()
            projet = Project(projectname="Machin", description="bidule")
            projet.save()
            Project(projectname="Machin2", description="bidule2").save()
            branche = Branch(branchname='ecolo',project=projet)
            branche.save()
            commentaire = Comment(comment="jusqu'ici tout va bien",commenttype=0, branch=branche, user=utilisateur)
            commentaire.save()
            request=HttpRequest()
            request.method = "POST"
            request.POST["comment"] = "salut ca va"
            request.POST["type"] = "1"
            response=afficher_branche(request,projet.id, branche.id)
            html = response.content.decode('utf8')
            self.assertIn("ici tout va bien",html)
            self.assertIn('ecolo',html)
            self.assertIn("salut ca va",html)

    def test_url_resolve_view_afficher_branche(self):
            utilisateur = User(username="Lucas", password="osef")
            utilisateur.save()
            projet = Project(projectname="Machin", description="bidule")
            projet.save()
            Project(projectname="Machin2", description="bidule2").save()
            branche = Branch(branchname='ecolo',project=projet)
            branche.save()
            commentaire = Comment(comment="jusqu'ici tout va bien",commenttype=0, branch=branche, user=utilisateur)
            commentaire.save()
            id_projet=projet.id
            id_branche=branche.id
            found = resolve('/projects/view/'+str(id_projet)+"/"+str(id_branche))
            self.assertEqual(found.func, afficher_branche)


