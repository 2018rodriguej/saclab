@csrf_exempt
def edit_project(req, project_id):
    project = Project.objects.filter(id=project_id)
    if not project:
        content = "Erreur : ce projet n'existe pas."
    else:
        if req.method == "POST":
            project = project[0]
            project.description = req.POST.get("description")
            project.save()
            content = "Le projet a été édité avec succès. % project.id
            content += "<br />Retour à la <a href=\"/projects\">liste des projets</a>."
        else:
            content = """Editer le projet %s. <br />
            <form action="/projects/edit/%s" method="post">
                <input type="text" name="description" value="%s" /><br />
                <input type="submit">
            </form>""" % (project_id, project_id, description)
    return HttpResponse(content)