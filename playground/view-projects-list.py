def afficher_liste_projets(request):
    project_list = "Liste des projets:<br/>"
    for project in Project.objects.all():
        project_url = "\"/projects/view/" + str(project.id) + "/\""
        project_list += " <a href=" + project_url + ">" + str(project.projectname) + "</a><br/>"
    project_list += "<br/><br/><br/><br/><br/><br/><br/><br/><a href=\"/projects/create/\">Créer un nouveau projet</a>"
    return HttpResponse(project_list)
