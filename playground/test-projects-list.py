class Liste_projectsPageTest(TestCase):

    def test_root_url_resolves_to_liste_projects_page_view(self):
        found = resolve('/projects/')
        self.assertEqual(found.func,afficher_liste_projets)

    def test_liste_produits_page_returns_correct_html(self):
        request = HttpRequest()
        response = afficher_liste_projets(request)
        html = response.content.decode('utf8')
        project=""
        project_url=""
        for project in Project.objects.all():
            project_url = "\"/projects/view/" + str(project.id) + "/\""
            project = " <a href=" + project_url + ">" + str(project.projectname) + "</a><br/>"
            self.assertContains(html, project)