from projects.models import Project, User, Work

def create_project(req):
    if req.method == "POST":
        project_name, project_description = req.POST.get("name"), req.POST.get("description")
        if not project_name or not project_description:
            content = "Erreur dans votre requête."
        else:
            try:
                project = Project(projectname=project_name, description=project_description)
                project.save()
                work = Work(id_user=1, id_project=project.id)
                work.save()
                content = "Votre nouveau projet a été créé. (identifiant: %s)" % project.id
                content += "<br />Retour à la <a href=\"/projects\">liste des projets</a>."
            except:
                content = "Erreur inconnue."
    else:
        content = """Créer un nouveau projet. <br />
        <form action="/projects/create" method="post">
            <input type="text" name="name" value="Nom du projet" /><br />
            <input type="text" name="description" value="Description /><br />
            <input type="submit">
        </form>"""
    return HttpResponse(content)