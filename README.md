# Saclab

Saclab est un outil de collaboration en ligne pour la gestion de projets.

### Notes

Couverture finale : 83%.

Pour lancer le test de couverture :
`coverage run --source='.' manage.py test && coverage report`