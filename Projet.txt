Projet du groupe 4
Membres : Swann Roussillon, Jules Rodriguez, Lucas Pirard, Valentino Ricotta, Nicolas Gasnier

* Sprint 1 : Elaboration de l'architecture generale du site
  Fonctionnalite 1 : Creation de l'application "Project" : Valentino
  Fonctionnalite 2 : Creation des differentes pages et des premieres vues ne necessitant pas l'utilisation d'un formulaire :
   * Affichage de la liste des projets : Nicolas
   * Affichage de l'index : Jules
   * Affichage des caracteristiques d'un projet : Lucas
   

* Sprint 2 : Ajout de fonctionnalites au site
  Fonctionnalite 1 : Elaboration de la base de donnees
   *  Creation de la base de donnees sur SQLite : Swann et Jules
   *  Incorporation de la base de donnees au projet Django : Swann
  Fonctionnalite 2 : Creation des vues necessitant l'utilisation d'un formulaire
   * Vue de creation d'un nouveau projet : Lucas et Valentino
   * Vu dedition d'un projet preexistant : Nicolas et Jules
       

* Sprint 3 : Finalisation du MVP
  Fonctionnalite 1 : Ajout d'un theme pour soigner l'affichage du site (Tout le monde)
  Fonctionnalite 2 : Creation du PowerPoint (Tout le monde)
  (*vu de chaques branchers)
   * vu de création de branches
   * vu avec formulaire jout de commentaire
   *modification de la base de données en conséquence))