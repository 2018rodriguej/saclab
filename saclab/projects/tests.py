from django.test import TestCase


from django.urls import resolve
from django.test import TestCase
from projects.views import index
from django.http import HttpResponse, HttpRequest
from projects.views import *
from projects.models import User, Project, Branch, Comment

class Liste_projectsPageTest(TestCase):
    
    # Test : existence de la route /projects
    def test_root_url_resolves_to_liste_projects_page_view(self):
        found = resolve('/projects/')
        self.assertEqual(found.func,afficher_liste_projets)

    # Test : l'affichage liste des projets fonctionne
    def test_liste_projects_page_returns_correct_html(self):
        User(username="Lucas", password="osef").save()
        Project(projectname="Machin", description="bidule").save()
        Project(projectname="Machin2", description="bidule2").save()
        request = HttpRequest()
        response = afficher_liste_projets(request)
        html = response.content.decode('utf8')
        for project in Project.objects.all():
            self.assertTrue(project.projectname in html)
    
    # Test : existence des routes /projects/view/<id>
    def test_url_projects_view(self):
        User(username="Lucas", password="osef").save()
        Project(projectname="Machin", description="bidule").save()
        Project(projectname="Machin2", description="bidule2").save()
        for project in Project.objects.all():
            found = resolve('/projects/view/'+str(project.id)+'/')
            self.assertEqual(found.func, afficher_projets)
        for id in range(100):
            found = resolve('/projects/view/'+str(id)+'/')
            self.assertEqual(found.func, afficher_projets)

    # Test : affichage de la description d'un projet et des branches
    def test_page_projects_view(self):
        u1 = User(username="Lucas", password="osef")
        u1.save()
        Project(projectname="Machin", description="bidule").save()
        Project(projectname="Machin2", description="bidule2").save()
        p3 = Project(projectname="Machin3", description="bidule3")
        p3.save()
        b31 = Branch(branchname="Machin-3.1", project=p3)
        b31.save()
        b32 = Branch(branchname='Machin-3.2', project=p3)
        b32.save()
        for project in Project.objects.all():
            request = HttpRequest()
            response = afficher_projets(request, project.id)
            html = response.content.decode('utf8')
            self.assertIn(project.projectname, html)
            self.assertIn(project.description, html)
            for branch in Branch.objects.filter(project=project):
                self.assertIn(branch.branchname, html)
        ids=[0,-1,len(Project.objects.all())+1]
        for id in ids:
            request = HttpRequest()
            response = afficher_projets(request, id)
            html = response.content.decode('utf8')
            self.assertTrue("Erreur" in html)
            self.assertTrue("Lucas" in html)
    
    # Test : existence de /projects/edit/<id>
    def test_url_resolve_edit_view(self):
        User(username="Lucas", password="osef").save()
        Project(projectname="Machin", description="bidule").save()
        Project(projectname="Machin2", description="bidule2").save()
        for k in range (len(Project.objects.all())):
            found = resolve('/projects/edit/'+str(k)+'/')
            self.assertEqual(found.func, edit_project)

    # Test : l'édition de projet fonctionne
    def test_edit_view_returns_right_message(self):
        User(username="Lucas", password="osef").save()
        Project(projectname="Machin", description="bidule").save()
        Project(projectname="Machin2", description="bidule2").save()
        request = HttpRequest()
        for k in range (len(Project.objects.all())):
            response = edit_project(request, k+1)
            html = response.content.decode('utf8')
            message = 'Editer'
            self.assertTrue(message in html)
        p = len(Project.objects.all()) + 5
        response = edit_project(request,p)
        html = response.content.decode('utf8')
        self.assertTrue("Erreur" in html)
        self.assertTrue("Lucas" in html)

class CreateProject_Test(TestCase):

    # Test : existence de /projects/create
    def test_create_project_route_resolves(self):
        User(username="Lucas", password="osef").save()
        found = resolve('/projects/create')
        self.assertEqual(found.func, create_project)
    
    # Test : le formulaire de création de projet fonctionne
    def create_project_returns_correct_html(self):
        User(username="Lucas", password="osef").save()
        request = HttpRequest()
        response = create_project(request)
        html = response.content.decode('utf8')
        self.assertTrue("nouveau projet" in html)

    # Test : la création d'un nouveau projet fonctionne
    def create_project_works(self):
        User(username="Lucas", password="osef").save()
        request = HttpRequest()
        request.method = "POST"
        request.POST["name"] = "Projet sympa"
        request.POST["description"] = "Description d'un projet sympa"
        response = create_project(request)
        html = response.content.decode('utf8')
        self.assertTrue("succès" in html)

        request = HttpRequest()
        request.method = "POST"
        request.POST["name"] = "Project missing a description argument (wtf??)"
        response = create_project(request)
        html = response.content.decode('utf8')
        self.assertTrue("Erreur" in html)
    
class index_page_tests(TestCase):

    # Test : la page d'accueil fonctionne
    def test_index_page_return_right_message(self):
        User(username="Lucas", password="osef").save()
        request = HttpRequest()
        response = index(request)
        html = response.content.decode('utf8')
        message = "Saclab"
        self.assertTrue(message in html)

    # Test : la page d'accueil existe
    def test_url_resolve_view_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    # Test : existence des /projects/add/<id> (ajout de branche à un projet)
    def test_url_resolve_view_ajouter_branche(self):
        User(username="Lucas", password="osef").save()
        Project(projectname="Machin", description="bidule").save()
        Project(projectname="Machin2", description="bidule2").save()
        Branch(branchname="Blabla", project = Project.objects.get(projectname="Machin")).save()
        Branch(branchname="Blabla2", project = Project.objects.get(projectname="Machin2")).save()
        for k in range (len(Project.objects.all())):
            found = resolve ('/projects/add/'+str(k+1)+'/')
            self.assertEqual(found.func, ajouter_branche)
    
    # Test : l'ajout de branche fonctionne
    def test_ajouter_branche_returns_right_message(self):
        User(username="Lucas", password="osef").save()
        Project(projectname="Machin", description="bidule").save()
        Project(projectname="Machin2", description="bidule2").save()
        for k in range(len(Project.objects.all())):
            request = HttpRequest()
            response = ajouter_branche(request, k+1)
            html = response.content.decode('utf8')
            self.assertTrue("Ajouter une branche" in html)

            request = HttpRequest()
            request.method = "POST"
            request.POST["name"] = "exemple de branche"
            response = ajouter_branche(request, k+1)
            html = response.content.decode('utf8')
            self.assertTrue("succès" in html)

            self.assertTrue(Branch.objects.filter(project=Project.objects.get(id=k+1)))

            request = HttpRequest()
            request.method = "POST"
            request.POST["wtf"] = "wtf is this"
            response = ajouter_branche(request, k+1)
            html = response.content.decode("utf8")
            self.assertTrue("Erreur" in html)

class afficher_branche_page_tests(TestCase):

    # Test : l'ajout et affichage de commentaires fonctionne
    def test_afficher_branche_page(self):
            utilisateur = User(username="Lucas", password="osef")
            utilisateur.save()
            projet = Project(projectname="Machin", description="bidule")
            projet.save()
            Project(projectname="Machin2", description="bidule2").save()
            branche = Branch(branchname='ecolo',project=projet)
            branche.save()
            commentaire = Comment(comment="jusqu'ici tout va bien",commenttype=0, branch=branche, user=utilisateur)
            commentaire.save()
            request=HttpRequest()
            request.method = "POST"
            request.POST["comment"] = "salut ca va"
            request.POST["type"] = "1"
            response=afficher_branche(request,projet.id, branche.id)
            html = response.content.decode('utf8')
            self.assertIn("ici tout va bien",html)
            self.assertIn('ecolo',html)
            self.assertIn("salut ca va",html)

    # Test : existence de /projects/view/<id>/<id_branche>
    def test_url_resolve_view_afficher_branche(self):
            utilisateur = User(username="Lucas", password="osef")
            utilisateur.save()
            projet = Project(projectname="Machin", description="bidule")
            projet.save()
            Project(projectname="Machin2", description="bidule2").save()
            branche = Branch(branchname='ecolo',project=projet)
            branche.save()
            commentaire = Comment(comment="jusqu'ici tout va bien",commenttype=0, branch=branche, user=utilisateur)
            commentaire.save()
            id_projet=projet.id
            id_branche=branche.id
            found = resolve('/projects/view/'+str(id_projet)+"/"+str(id_branche))
            self.assertEqual(found.func, afficher_branche)