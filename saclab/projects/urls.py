from django.conf.urls import url, include
from django.shortcuts import redirect
from projects import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^projects/?$', views.afficher_liste_projets, name='afficher_liste_projets'),
    url(r'^projects/view/?$', lambda req: redirect('/projects')),
    url(r'^projects/view/(?P<project_id>[0-9]+)/?$', views.afficher_projets, name='afficher_projets'),
    url(r'^projects/edit/?$', lambda req: redirect('/projects')),
    url(r'^projects/edit/(?P<project_id>[0-9]+)/?$', views.edit_project, name='edit_project'),
    url(r'^projects/create/?$', views.create_project, name='create_project'),
    url(r'^projects/view/(?P<project_id>[0-9]+)/(?P<branch_id>[0-9]+)/?$', views.afficher_branche, name='afficher_branche'),
    url(r'^projects/add/(?P<project_id>[0-9]+)/?$', views.ajouter_branche, name='ajouter_branche'),
]
