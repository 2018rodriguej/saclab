from django.db import models

# Create your models here.
class User(models.Model):
    username=models.CharField(max_length=200, unique=True)
    password=models.CharField(max_length=200, unique=False)

class Project(models.Model):
    projectname=models.CharField(max_length=200,unique=False)
    description=models.CharField(max_length=600,unique=False)

class Branch(models.Model):
    branchname=models.CharField(max_length=400,unique=False)
    project=models.ForeignKey(Project, on_delete=models.CASCADE)     #crée le lien entre Project[1,n]-[1,1]Branch


class Work(models.Model):
    user=models.ForeignKey(User, on_delete=models.CASCADE)
    projet=models.ForeignKey(Project, on_delete=models.CASCADE)     #crée le lien entre Project[1,n]-[1,1]Work

class Comment(models.Model):
    comment=models.CharField(max_length=1000,unique=False)
    commenttype=models.IntegerField()
    branch=models.ForeignKey(Branch, on_delete=models.CASCADE)
    user=models.ForeignKey(User, on_delete=models.CASCADE)    #crée le lien entre User[0,n]-[1,1]Commment





