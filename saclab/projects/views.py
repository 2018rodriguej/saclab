from django.shortcuts import render
from django.http import HttpRequest, HttpResponse
from django.utils.http import urlencode
from django.template import loader
from projects.models import Project, User, Work, Branch, Comment
from django.views.decorators.csrf import csrf_exempt

import html

# On crée ici la page d'accueil de notre site 
def index(request):
    template = loader.get_template("./projects/index.html")
    return HttpResponse(template.render(request=request, context={'username': User.objects.get(id=1).username}))


# On souhaite ici présenter la liste de tout les projets à l'utilisateur 
def afficher_liste_projets(request):
    # On parcourt la liste des projets pour la passer au template
    projects = [{'id': project.id, 'projectname': project.projectname} for project in Project.objects.all()]
    template = loader.get_template("./projects/list.html")  
    return HttpResponse(template.render(request=request, context={'username': User.objects.get(id=1).username,
                                                                  'projects' : projects}))


# On enlève la protection CSRF présente par défaut dans Django
@csrf_exempt
def create_project(req):
    # Si on a envoyé des données par le formulaire
    if req.method == "POST":
        project_name, project_description = req.POST.get("name"), req.POST.get("description")
        # Si on a pas passé les bons paramètres, erreur
        if not project_name or not project_description:
            content = "Erreur dans votre requête."
            template = loader.get_template("./projects/general.html")
            return HttpResponse(template.render(request=req, context={'username': User.objects.get(id=1).username,
                                                                      'content': content}))
        else:
            project = Project(projectname=project_name, description=project_description)
            project.save()
            # Vu qu'on a pas implementé le système d'authentification encore,
            # l'utilisateur par défaut est Lucas, d'identifiant 1
            user = User.objects.get(username="Lucas")
            work = Work(user=user, projet=project)
            work.save()
            content = "Votre nouveau projet a été créé avec succès."
            content += "<br />Retour à la <a href=\"/projects\">liste des projets</a>."
            template = loader.get_template("./projects/general.html")
            return HttpResponse(template.render(request=req, context={'username': User.objects.get(id=1).username,
                                                                      'content': content}))
    else:
        template = loader.get_template("./projects/create.html")
        return HttpResponse(template.render(request=req, context={'username': User.objects.get(id=1).username}))

# On définit une fonction pour afficher des informations sur un projet
def afficher_projets(request, project_id):
    project = Project.objects.filter(id=project_id)
    # Si le projet passé en URL n'existe pas, erreur
    if not project:
        content = "Erreur : ce projet n'existe pas."
        template = loader.get_template("./projects/general.html")
        return HttpResponse(template.render(request=request, context={'username': User.objects.get(id=1).username,
                                                                      'content': content}))
    else:
        project = project[0]
        branches = Branch.objects.filter(project=project.id)
        branches_dicts = [{'branchid': branch.id,
                           'branchname': branch.branchname,
                           'n_comments': len(Comment.objects.filter(branch=branch))} for branch in branches]
        template = loader.get_template("./projects/view.html")
        return HttpResponse(template.render(request=request, context={'username': User.objects.get(id=1).username,
                                                                      'id': project.id,
                                                                      'projectname': project.projectname,
                                                                      'description': project.description,
                                                                      'branches': branches_dicts}))


@csrf_exempt
def edit_project(request, project_id):
    project = Project.objects.filter(id=project_id)
    if not project:
        content = "Erreur : ce projet n'existe pas."
        template = loader.get_template("./projects/general.html")
        return HttpResponse(template.render(request=request, context={'username': User.objects.get(id=1).username,
                                                                      'content': content}))
    else:
        project = project[0]
        # Si on a envoyé une édition de description
        if request.method == "POST":
            project.description = request.POST.get("description")
            project.save()
            content = "Le projet a été édité avec succès"
            content += "<br />Retour à la <a href=\"/projects/view/%s\">page du projet</a>." % project.id
            content += "<br />Retour à la <a href=\"/projects\">liste des projets</a>."
            template = loader.get_template("./projects/general.html")
            return HttpResponse(template.render(request=request, context={'username': User.objects.get(id=1).username,
                                                                        'content': content}))
        else:
            template = loader.get_template("./projects/edit.html")
            return HttpResponse(template.render(request=request, context={'username': User.objects.get(id=1).username,
                                                                          'id': project.id,
                                                                          'projectname': project.projectname,
                                                                          'description': project.description}))
    return HttpResponse(content)

@csrf_exempt
def ajouter_branche(request, project_id):
    project = Project.objects.filter(id=project_id)
    # Si le projet n'existe pas, erreur
    if not project:
        content = "Erreur : ce projet n'existe pas."
        template = loader.get_template("./projects/general.html")
        return HttpResponse(template.render(request=request, context={'username': User.objects.get(id=1).username,
                                                                      'content': content}))
    else:
        project = project[0]
        if request.method == "POST":
            branchname = request.POST.get("name")
            if not branchname:
                content = "Erreur dans votre requête."
                template = loader.get_template("./projects/general.html")
                return HttpResponse(template.render(request=request, context={'username': User.objects.get(id=1).username,
                                                                              'content': content}))
            else:
                branch = Branch(branchname=branchname, project=project)
                branch.save()
                content = "La nouvelle branche a été créée avec succès."
                content += "<br />Voir la <a href=\"/projects/view/%s/%s\">branche</a>." % (project.id, branch.id)
                content += "<br />Retour au <a href=\"/projects/view/%s\">projet</a>." % project.id
                template = loader.get_template("./projects/general.html")
                return HttpResponse(template.render(request=request, context={'username': User.objects.get(id=1).username,
                                                                            'content': content}))
        else:
            template = loader.get_template("./projects/addbranch.html")
            return HttpResponse(template.render(request=request, context={'username': User.objects.get(id=1).username,
                                                                          'id': project.id,
                                                                          'projectname': project.projectname}))

@csrf_exempt 
def afficher_branche(request, project_id, branch_id):
    project = Project.objects.filter(id=project_id)
    # Si le projet n'existe pas, erreur
    if not project:
        content = "Erreur : ce projet n'existe pas."
        template = loader.get_template("./projects/general.html")
        return HttpResponse(template.render(request=request, context={'username': User.objects.get(id=1).username,
                                                                    'content': content}))
    else:
        project = project[0]
        branch = Branch.objects.filter(project=project, id=branch_id)
        # Si la branche n'existe pas, erreur
        if not branch:
            content = "Erreur : cette branche n'existe pas."
            template = loader.get_template("./projects/general.html")
            return HttpResponse(template.render(request=request, context={'username': User.objects.get(id=1).username,
                                                                        'content': content}))
        else:
            branch = branch[0]
            new_comment = False
            # Si on a envoyé un nouveau commentaire
            if request.method == "POST":
                # On récupère les données de la requête
                commenttype, commenttext = request.POST.get("type"), request.POST.get("comment")
                error = False
                try:
                    assert commenttype and commenttext
                    # Le type est un entier entre 0 et 3 (transparent pour l'utilisateur)
                    commenttype = int(commenttype)
                    assert 0 <= commenttype <= 3
                    # 0 = Information
                    # 1 = Suggestion
                    # 2 = Problème
                    # 3 = Solution
                except:
                    error = True
                if error:
                    content = "Erreur dans votre requête."
                    template = loader.get_template("./projects/general.html")
                    return HttpResponse(template.render(request=request, context={'username': User.objects.get(id=1).username,
                                                                                  'content': content}))
                else:
                    comment = Comment(commenttype=commenttype, branch=branch, user=User.objects.get(id=1), comment=commenttext)
                    comment.save()
                    new_comment = True
            
            # On passe toutes les informations qu'il faut au template
            comments = Comment.objects.filter(branch=branch)
            comment_dicts = [{'username': comment.user.username,
                              'type': comment.commenttype,
                              'comment': comment.comment} for comment in comments]
            template = loader.get_template("./projects/branch.html")
            return HttpResponse(template.render(request=request, context={'username': User.objects.get(id=1).username,
                                                                          'projectid': project.id,
                                                                          'branchid': branch.id,
                                                                          'projectname': project.projectname,
                                                                          'branchname': branch.branchname,
                                                                          'n_comments': len(comments),
                                                                          'comments': comment_dicts,
                                                                          'new_comment': new_comment}))